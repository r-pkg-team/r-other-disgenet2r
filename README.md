# disgenet2r

`disgenet2r` is an R package to query and expand DisGeNET data (www.disgenet.org), and to visualize the results within R framework.
The disgenet2r is designed to query data for DisGeNET v7.0 (May, 2020).      


# IMPORTANT: The disgenet2r package is no longer updated and will soon be replaced by [disgenetplus2r](https://gitlab.com/medbio/disgenetplus2r). 

A detailed documentation of the **disgenetplus2r** is available [here](https://medbio.gitlab.io/disgenetplus2r/).



## Package' Status

 * __Version__: 0.99.3
 * __Subversion__: `20230223`
 * __Authors__:  IBI group
 * __Maintainer__: <support@disgenet.org>


## COPYRIGHT

Copyright (C) 2023 IBI group.

disgenet2r is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

disgenet2r is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

## How to cite disgenet2r:

Piñero, J., Ramírez-Anguita, J. M., Saüch-Pitarch, J., Ronzano, F., Centeno, E., Sanz, F., & Furlong, L. I. (2020). The DisGeNET knowledge platform for disease genomics: 2019 update. Nucleic acids research, 48(D1), D845-D855.


